import json

from collections import Counter


def main():
    data = None
    with open("./results.json", "r") as f:
        data = json.loads(f.read())
    orgs = Counter()
    for _, v in data["data"].items():
        orgs.update(ip["org"] for ip in v["ips"])
        for ns in v["nameservers"].values():
            orgs.update(ip["org"] for ip in ns)
    print(orgs.most_common(9))


if __name__ == "__main__":
    main()
