import json
from datetime import datetime
import dns.resolver
import requests
import progressbar


class DataAcquisition:
    def __init__(self):
        self.my_reso = dns.resolver.Resolver()
        self.my_reso.nameservers = ["80.67.169.12", "80.67.169.40"]

        websites = []
        with open("./domains.txt", "r") as f:
            for website in f:
                websites.append(website.strip())
        alls = {}
        for website in progressbar.progressbar(websites):
            alls[website] = self.resolve(website)
            alls[website]["ips"] = [self.get_ip_info(ip) for ip in alls[website]["ips"]]
            for ns, ips in alls[website]["nameservers"].items():
                alls[website]["nameservers"][ns] = [self.get_ip_info(ip) for ip in ips]
            with open("./results.json", "w") as f:
                f.write(
                    json.dumps(
                        {
                            "meta": {
                                "created_at": datetime.now().isoformat(),
                                "resolvers": self.my_reso.nameservers,
                            },
                            "data": alls,
                        }
                    )
                )

    def get_ip_info(self, ip):
        res = requests.get("https://ipinfo.io/{}/json".format(ip))
        res.raise_for_status()
        return res.json()

    def resolve_ips(self, domain):
        ipv6 = True
        try:
            ips = [str(r) for r in self.my_reso.query(domain, "A")]
        except dns.resolver.NoAnswer:
            ips = []
        try:
            ips.extend(str(r) for r in self.my_reso.query(domain, "AAAA"))
        except dns.resolver.NoAnswer:
            ipv6 = False
        return ips, ipv6

    def resolve(self, domain):
        res = {"ips": [], "nameservers": {}}
        ips, ipv6 = self.resolve_ips(domain)
        res["ips"].extend(ips)
        res["ipv6"] = bool(ipv6)
        try:
            for ns in self.my_reso.query(domain, "NS"):
                ns_ips, _ = self.resolve_ips(str(ns))
                res["nameservers"][str(ns)] = ns_ips
        except dns.resolver.NoAnswer:
            pass
        return res


def main():
    DataAcquisition()


if __name__ == "__main__":
    main()
